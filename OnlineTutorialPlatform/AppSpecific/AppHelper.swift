//
//  AppHelper.swift
//  ShortTrip
//
//  Created by prompt on 26/05/17.
//  Copyright © 2017 Prompt Softech. All rights reserved.
//

import UIKit
import SystemConfiguration
import EventKit
class AppHelper: NSObject {

    static func localizedtext(key:String) -> String {
        return NSLocalizedString(key, comment: "")
    }
    
    static func addEventToCalendar(title: String, description: String?, startDate: Date, endDate: Date, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                    let reminder = EKAlarm(relativeOffset: 3600)
                    event.addAlarm(reminder)
                } catch let e as NSError {
                    completion?(false, e)
                    return
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }
    
    static func createNoRecordView(superView : UIView?, message : String?, imageName : String?) -> UIView {
        let view = self.createNoRecordView(superView: superView, message: message, imageName: imageName, topMargin: -20)
        view.backgroundColor = COLOR.titleColor;
        return view;
    }
    
    static func createNoRecordView(superView : UIView?, message : String?, imageName : String?, topMargin: CGFloat) -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = COLOR.backGroundColor;
        view.isHidden = true
        superView?.addSubview(view)
        
        // Add constraints to superview
        superView?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: ["view" : view]))
        superView?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: ["view" : view]))
        
        // Create subviews
        let imageView = UIImageView(image: UIImage.init(named: imageName ?? ""))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.alpha = 0.7
        view.addSubview(imageView)
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        label.text = message
        label.textAlignment = .center
        label.font = Font.titleFont
        label.textColor = UIColor(named: "fontColor")
        label.numberOfLines = 0
        
        let views = ["imageView" : imageView, "label" : label]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[imageView(40)]", options: [], metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[imageView(40)]", options: [], metrics: nil, views: views))
        
        view.addConstraint(NSLayoutConstraint.init(item: imageView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint.init(item: label, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0))
        
        // vertical constraints
        view.addConstraint(NSLayoutConstraint.init(item: label, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: topMargin))
        
        view.addConstraint(NSLayoutConstraint.init(item: imageView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: label, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: -50))
        view.backgroundColor = COLOR.backGroundColor;
        return view;
    }
    
   
    
    
    static func handleInvalidToken(){
        DispatchQueue.main.async {
//            GlobalUtils.getInstance().setSessionToken(token: "")
//            GlobalUtils.getInstance().setEmail(email: "")
//            GlobalUtils.getInstance().setUserID(userID: "");
//            GlobalUtils.getInstance().setLogin(islogin: false)
//            GlobalUtils.getInstance().setUserType(user_type:"")
//            let mainStoryboard = storyBoard.kMainStoryboard;
//            let loginVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//            let navVC = UINavigationController(rootViewController: loginVC);        kAppDelegate.window?.rootViewController = navVC;
        }
    }
    //---------------------------------------------------------------------
    // MARK: Check Internet Connected or Not
    //---------------------------------------------------------------------
    static func isInternetConnected() -> Bool{
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
}

