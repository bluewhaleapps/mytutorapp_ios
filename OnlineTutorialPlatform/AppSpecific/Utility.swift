//
//  Utility.swift
//  SplashTrack
//
//  Created by promptmac on 21/06/18.
//  Copyright © 2018 Prompt. All rights reserved.
//

import UIKit
import Foundation
import CommonCrypto

class Utility: NSObject {
    static func generateSignatureHMACHASWithAPIKey(apiKey:String,apiSecretKey:String,userId:String) -> Dictionary<String,String>{
        
        let uIdentifier = UIDevice.current.identifierForVendor!.uuidString;
        let nounce = self.getNonce();
        let signatureRaw = "\(apiKey):\(nounce):\(userId)";
        let cKey =  apiSecretKey.cString(using: String.Encoding.utf8)
        let cData =  signatureRaw.cString(using: String.Encoding.utf8)
        var result = [CUnsignedChar](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
        CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA256), cKey!, strlen(cKey!), cData!, strlen(cData!), &result)
        let hmacData:NSData = NSData(bytes: result, length: (Int(CC_SHA256_DIGEST_LENGTH)))
        let hmacBase64 = hmacData.base64EncodedString(options: .lineLength64Characters)
        var dictHmac : Dictionary<String,String> = [:]
        dictHmac["apiKey"] = apiKey;
        dictHmac["apiSecretKey"] = apiSecretKey;
        dictHmac["userId"] = userId;
        dictHmac["hmacHash"] = hmacBase64;
        dictHmac["nonce"] = nounce;
        dictHmac["uIdentifier"] = uIdentifier;
        return dictHmac;
    }
    
   static func getNonce() -> String{
        var nounce = self.getUUID();
        nounce = nounce.replacingOccurrences(of: "-", with: "")
        return nounce;
    }
    
   static func getUUID() -> String{
        var uuid = UUID().uuidString
        let index1 = uuid.index(uuid.startIndex, offsetBy: 10)
        uuid = uuid.substring(to: index1)
        return uuid;
    }
    
}
