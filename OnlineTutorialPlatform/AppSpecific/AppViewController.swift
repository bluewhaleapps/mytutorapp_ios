//
//  AppViewController.swift
//  ShortTrip
//
//  Created by prompt on 26/05/17.
//  Copyright © 2017 Prompt Softech. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SVProgressHUD
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

class AppViewController: UIViewController,UIGestureRecognizerDelegate {
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let picker = UIPickerView()
    var refreshControl = UIRefreshControl()
    let toolBar = UIToolbar();

    var societyParam:Dictionary<String,Any> = [:]
    
    // MARK: View Life Cycle ------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "FIU BEHAVIORAL TRACKING APP";
        let appearance = UITableView.appearance()
        appearance.tableFooterView = UIView(frame: CGRect.zero);
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: COLOR.navTintColor,
             NSAttributedString.Key.font: Font.titleFont]
        self.navigationController?.navigationBar.isTranslucent = false;
        self.navigationController?.navigationBar.barTintColor = COLOR.navBarColor
        self.navigationController?.navigationBar.tintColor = COLOR.navTintColor;
        self.view.backgroundColor = COLOR.backGroundColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    //---------------------------------------------------------------------
    // MARK: Round Image
    //---------------------------------------------------------------------
    func RoundImage(img: UIImageView)  {
        img.layer.cornerRadius = img.frame.size.width / 2
        img.clipsToBounds = true
        img.contentMode = .scaleToFill
    }
    func isValidEmail(textStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: textStr)
    }
    
    func isValidPasswordFormat(textStr: String) -> Bool {
        let pwRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,15}"
        let pwTest = NSPredicate(format:"SELF MATCHES %@", pwRegEx)
        return pwTest.evaluate(with: textStr)
    }
    
    func startAnimating(){
        self.view.isUserInteractionEnabled = false
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
       
    }
    func stopAnimating(){
        self.view.isUserInteractionEnabled = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
       
    }
    
    //---------------------------------------------------------------------
    // MARK: Handle Invalid Token
    //---------------------------------------------------------------------
    

    //---------------------------------------------------------------------
    // MARK: Alert View
    //---------------------------------------------------------------------
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title:title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func  progressColor(value:Int) -> UIColor {
        if value < 70 {
            return UIColor.red
        }
        else if value >= 70 && value < 85 {
            return UIColor.yellow
        }
        else if value >= 85 && value < 100 {
            return UIColor.blue
        }
        else{
            return UIColor.green
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func showActivityIndicatory(isLoading:Bool = true) {
        SVProgressHUD.show(withStatus: "Loading...")
    }
    func hideActivityIndicator() {
        SVProgressHUD.dismiss()
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

