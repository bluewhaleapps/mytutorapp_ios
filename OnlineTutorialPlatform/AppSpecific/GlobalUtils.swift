//
//  Global Utility.swift
//  ShortTrip
//
//  Created by Abhishek Yadav on 17/05/17.
//  Copyright © 2017 Prompt Softech. All rights reserved.84
//

import Foundation

class GlobalUtils {
    
    fileprivate static var globalUtils:GlobalUtils?
    
    init() {
    }
    
    //---------------------------------------------------------------------
    // MARK: Create
    //---------------------------------------------------------------------
    static func create() -> GlobalUtils{
        if(GlobalUtils.globalUtils == nil){
            GlobalUtils.globalUtils = GlobalUtils()
        }
        return GlobalUtils.globalUtils!
    }
    
    //---------------------------------------------------------------------
    // MARK: Get Instance
    //---------------------------------------------------------------------
    static func getInstance() -> GlobalUtils{
        if(GlobalUtils.globalUtils == nil){
            GlobalUtils.globalUtils = GlobalUtils.create()
        }
        return GlobalUtils.globalUtils!
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    //---------------------------------------------------------------------
    // MARK: Set and Get Email
    //---------------------------------------------------------------------
    
    func setEmail(email:String) -> Void {
        UserDefaults.standard.set(email, forKey: "email")
        UserDefaults.standard.synchronize()
    }
    func setWantToRefer(firstname:String,lastname:String,email:String,phone:String)-> Void {
        let dict:Dictionary = ["firstname":firstname,"lastname":lastname,"email":email,"phone":phone]
        UserDefaults.standard.set(dict, forKey: "wantTorefer")
        UserDefaults.standard.synchronize()
    }
    func getWantToRefer() -> Dictionary<String, String>  {
        let dict:Dictionary = UserDefaults.standard.value(forKey: "wantTorefer") as! Dictionary<String, String>
        return dict as! Dictionary<String, String>
    }
    
    func email() -> String {
        var userName = UserDefaults.standard.value(forKey: "email")
        if userName == nil {
            userName = ""
        }
        return userName as! String
    }
    
    
    func setClientJobTitleId(ClientJobTitleId:String) -> Void {
        UserDefaults.standard.set(ClientJobTitleId, forKey: "ClientJobTitleId")
        UserDefaults.standard.synchronize()
    }
    
    func getClientJobTitleId() -> String {
        var ClientJobTitleId = UserDefaults.standard.value(forKey: "ClientJobTitleId")
        if ClientJobTitleId == nil {
            ClientJobTitleId = ""
        }
        return ClientJobTitleId as! String
    }
    
   
    func username() -> String {
        var userName = UserDefaults.standard.value(forKey: "username")
        if userName == nil {
            userName = ""
        }
        return userName as! String
    }
    
    
    func setUserName(username:String) -> Void {
        UserDefaults.standard.set(username, forKey: "username")
        UserDefaults.standard.synchronize()
    }
    
    
    func password() -> String {
        var password = UserDefaults.standard.value(forKey: "password")
        if password == nil {
            password = ""
        }
        return password as! String
    }
    
    
    
    
    func setPassword(password:String) -> Void {
        UserDefaults.standard.set(password, forKey: "password")
        UserDefaults.standard.synchronize()
    }
    
    func designation() -> String {
        var userName = UserDefaults.standard.value(forKey: "JobTitleName")
        if userName == nil {
            userName = ""
        }
        return userName as! String
    }
    
    func setDesignation(designation:String) -> Void {
        UserDefaults.standard.set(designation, forKey: "JobTitleName")
        UserDefaults.standard.synchronize()
    }
    
    func clientID() -> String {
        var clientID = UserDefaults.standard.value(forKey: "clientID")
        if clientID == nil {
            clientID = ""
        }
        return clientID as! String
    }
    
    func setClientID(clientId:String)  {
        UserDefaults.standard.set(clientId, forKey: "clientID")
        UserDefaults.standard.synchronize()
    }
    
    
    func setUserDetailID(userDetailID:String) -> Void {
        UserDefaults.standard.set(userDetailID, forKey: "userDetailID")
        UserDefaults.standard.synchronize()
    }
    
    func userDetailID() -> String {
        var userID = UserDefaults.standard.value(forKey: "userDetailID")
        if userID == nil {
            userID = ""
        }
        return userID as! String
    }
    
    
    func setUserID(userID:String) -> Void {
        UserDefaults.standard.set(userID, forKey: "userID")
        UserDefaults.standard.synchronize()
    }
    
    func userID() -> String {
        var userID = UserDefaults.standard.value(forKey: "userID")
        if userID == nil {
            userID = ""
        }
        return userID as! String
    }
    
    //---------------------------------------------------------
    // MARK: Set and Get Session Token
    //---------------------------------------------------------
    
    func setSessionToken(token:String) -> Void {
        UserDefaults.standard.set(token, forKey: "token")
        UserDefaults.standard.synchronize()
    }
    func setUserType(user_type:String) -> Void {
        UserDefaults.standard.set(user_type, forKey: "user_type")
        UserDefaults.standard.synchronize()
    }
    func isInfluencer() -> Bool {
        let user_type = UserDefaults.standard.value(forKey: "user_type") as? String ?? ""
        var isInfluencer = false;
        if user_type == "Influencer" {
            isInfluencer = true;
        }else if user_type == ""{
            isInfluencer = false
        }
        //return false;
        return isInfluencer
        
    }
    func setApplicationLanguage(language:String)  {
        UserDefaults.standard.set(language, forKey: "selectedLanguage")
        UserDefaults.standard.synchronize()
    }
    func getApplicationLanguage() -> String {
        let langName =  UserDefaults.standard.value(forKey: "selectedLanguage") as? String ?? ""
        return langName;
    }
    func setlangShortName(language:String)  {
      //  [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@""];
        UserDefaults.standard.set(language, forKey: "AppleLanguages")
        UserDefaults.standard.set(language, forKey: "shortLangName")
        UserDefaults.standard.synchronize()
    }
    func langShortName() -> String {
        let langName =  UserDefaults.standard.value(forKey: "shortLangName") as? String ?? ""
        return langName;
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func sessionToken() -> String {
        var sessionToken = UserDefaults.standard.value(forKey: "token")
        if sessionToken == nil {
            sessionToken = ""
        }
       // sessionToken = "0647ccc3-c972-4a9b-981b-10d7bf7abfb8"
      // sessionToken = "LoginToken-Influencer"
        return sessionToken as! String
    }
    
    
    func setDeviceToken(token:String) -> Void {
        UserDefaults.standard.set(token, forKey: "devicetoken")
        UserDefaults.standard.synchronize()
    }
    
    func getDeviceToken() -> String {
        var deviceToken = UserDefaults.standard.value(forKey: "devicetoken")
        if deviceToken == nil {
            deviceToken = ""
        }
        
        return deviceToken as! String
    }
    
    
    // Mark: set and get API Secret and API Key ---------------------------
  
    
    
    func setApiKey(ApiKey:String) -> Void {
        UserDefaults.standard.set(ApiKey, forKey: "ApiKey")
        UserDefaults.standard.synchronize()
    }
    
    func ApiKey() -> String {
        var ApiKey = UserDefaults.standard.value(forKey: "ApiKey")
        if ApiKey == nil {
            ApiKey = ""
        }
        return ApiKey as! String
    }
    
    func setAPISecret(APISecret:String) -> Void {
        UserDefaults.standard.set(APISecret, forKey: "APISecret")
        UserDefaults.standard.synchronize()
    }
    
    func APISecret() -> String {
        var APISecret = UserDefaults.standard.value(forKey: "APISecret")
        if APISecret == nil{
            APISecret = ""
        }
        return APISecret as! String
    }
    
    
    func setlstClientModule(clientModule:[[String:Any]]) -> Void {
        UserDefaults.standard.set(clientModule, forKey: "lstClientModuleMapping")
        UserDefaults.standard.synchronize()
    }
    
    func clientModule() -> [[String:Any]] {
        var clientModule = UserDefaults.standard.value(forKey: "lstClientModuleMapping")
        if clientModule == nil{
            clientModule = []
        }
        return clientModule as! [[String : Any]] 
    }
    
    func setDrawinID(drawinId:String) -> Void {
        UserDefaults.standard.set(drawinId, forKey: "DrawinId")
        UserDefaults.standard.synchronize()
    }
    
    func drawinId() -> String {
        var DrawinId = UserDefaults.standard.value(forKey: "DrawinId")
        if DrawinId == nil{
            DrawinId = ""
        }
        return DrawinId as! String
    }
    
    
    
    func setProfilePic(profilePic:String) -> Void {
        UserDefaults.standard.set(profilePic, forKey: "profilePic")
        UserDefaults.standard.synchronize()
    }
    
    func profilePic() -> String {
        var profilePic = UserDefaults.standard.value(forKey: "profilePic")
        if profilePic == nil{
            profilePic = ""
        }
        return profilePic as! String
    }
    
    func setUserDesignation(designation:String) -> Void {
        UserDefaults.standard.set(designation, forKey: "designation")
        UserDefaults.standard.synchronize()
    }
    
    func userDesignation() -> String {
        var designation = UserDefaults.standard.value(forKey: "designation")
        if designation == nil{
            designation = ""
        }
        return designation as! String
    }
    
    
    
    func setStatesArray(stateArray:[[String:Any]]) -> Void {
        UserDefaults.standard.set(stateArray, forKey: "state")
        UserDefaults.standard.synchronize()
    }
    
    func getStatesArray() -> [[String:Any]] {
        var state = UserDefaults.standard.value(forKey: "state") as? [[String:Any]] ;
        if state == nil{
            state = []
        }
        return state!;
    }
    
    
    
    
    func getAppVersion() -> String{
        if Bundle.main.infoDictionary != nil{
            let data = Bundle.main.infoDictionary
            if let version = data?["CFBundleShortVersionString"]{
                return version as! String
            }
        }
        return "";
    }
    
    //---------------------------------------------------------------------
    // MARK: Set and Get Login
    //---------------------------------------------------------------------
    
    func setLogin(islogin:Bool) -> Void {
        UserDefaults.standard.set(islogin, forKey: "isLoginOrNot")
        UserDefaults.standard.synchronize()
    }
    
    func GetLogin() -> Bool {
        var isLoginOrNot = UserDefaults.standard.bool(forKey: "isLoginOrNot")
        if isLoginOrNot == nil{
            isLoginOrNot = false
        }
        return isLoginOrNot;
    }
    
    //---------------------------------------------------------------------
    // MARK: Set and Get Login
    //---------------------------------------------------------------------
    
    func setifUserLoggedIn(islogin:Bool) -> Void {
        UserDefaults.standard.set(islogin, forKey: "userLoggedIn")
        UserDefaults.standard.synchronize()
    }
    
    func userLoggedIn() -> Bool {
        var userLoggedIn = UserDefaults.standard.bool(forKey: "userLoggedIn")
        if userLoggedIn == nil{
            userLoggedIn = false
        }
        return userLoggedIn;
       // return UserDefaults.standard.value(forKey: "") as! Bool ?? false
    }
    
    
    func getHeaderParam() -> Dictionary<String,String>{
        var headerParam =  [WebServiceCall.KContentType:WebServiceCall.KContentValue,]
        var token = GlobalUtils.getInstance().sessionToken();
        let countryCode = NSLocale.current.regionCode
        let currencyCode = NSLocale.current.currencyCode
        
       // token = "LoginToken-Influencer"
        if(token != ""){
            headerParam ["AuthorizationToken"] = token
        }
        let langShortName = self.langShortName()
        var identifier = "en";
        switch langShortName {
        case "en":
            identifier = "en-US"
            break
        case "es":
            identifier = "es-ES"
            break
        case "es":
            identifier = "ru-RU"
            break
        case "es":
            identifier = "pt-PT"
            break
        default:
            identifier = "en-US"
            
        }
        headerParam["CultureInfo"] = identifier
        headerParam["Currency"] = "USD";
        headerParam["Country"] = "USA"
        return headerParam;
    }
    
    
    
    //MARK:- StartDate & EndDate
    func GetDays() -> Int {
        return UserDefaults.standard.value(forKey: "days") as? Int ?? 30
    }
    func setAppStartDate(dateStart:String) -> Void {
        UserDefaults.standard.set(dateStart, forKey: "startDate")
        UserDefaults.standard.synchronize()
    }
    func setAppEndDate(dateEnd:String) -> Void {
        UserDefaults.standard.set(dateEnd, forKey: "EndDate")
        UserDefaults.standard.synchronize()
    }
    func getStartDate() -> String {
        return UserDefaults.standard.value(forKey: "startDate") as! String
    }
    func getEndDate() -> String {
        return UserDefaults.standard.value(forKey: "EndDate") as! String
    }
    
    func setLoginDateTime(datelogind:Date) -> Void {
        UserDefaults.standard.set(datelogind, forKey: "logindate")
        UserDefaults.standard.synchronize()
    }
    func getLoginDateTime() -> Date {
        return UserDefaults.standard.value(forKey: "logindate") as! Date
    }
    func setLogoutAlert(islogout:Bool) -> Void {
        UserDefaults.standard.set(islogout, forKey: "securityLogout")
        UserDefaults.standard.synchronize()
    }
    func getLogoutAlert() -> Bool {
        return UserDefaults.standard.value(forKey: "securityLogout") as! Bool
    }
    
}

