//
//  Constant.swift
//  ShortTrip
//
//  Created by Abhishek Yadav on 13/05/17.
//  Copyright © 2017 Prompt Softech. All rights reserved.
//
import Foundation
import UIKit
//let bundleIdentifier = "com.GreetClub"
let kAppDelegate = UIApplication.shared.delegate as! AppDelegate;
let kAppName = Bundle.main.localizedInfoDictionary?["CFBundleName"] as? String ?? "FIU Behaviour Tracking"

typealias JSONObject = Dictionary<String, Any>
typealias JSONArray = [JSONObject]
typealias ID = String

struct storyBoard{
        static let kMainStoryboard = UIStoryboard(name: "Main", bundle: nil);
}

struct  WebServiceUrl{
    static let pageCount = 10;
   // static let kBASEURL = "http://e81767a0.ngrok.io/api/"
    static let kBASEURL = "http://staging.thegreetclub.com/api/"
}
struct WebServiceCall {
    
    static let KContentType = "Content-Type"
    static let KContentValue = "application/json"
    static let kAuthorizationToken = "AuthorizationToken"
}

struct Font {
    static let regularFont = UIFont(name: "Quicksand-Regular", size: 20)!;
    static let titleFont  = UIFont(name: "Quicksand-Bold", size: 18)!;
    static let barButtonFont  = UIFont(name: "Quicksand-Regular", size: 18)!;
    static let tabbarFont  = UIFont(name: "Quicksand-Regular", size: 12)!;
}


struct COLOR{
    static let navBarColor = UIColor(named: "navBarColor")!
    static let navTintColor = UIColor(named: "navTintColor")!
    static let titleColor = UIColor(named: "navBarColor")!
    static let backGroundColor = UIColor(named: "backgroundColor")!
    static let tabbarSelectedColor = UIColor(named: "fontColor")!
    static let buttonTitleColor = UIColor.white
    static let availableColor = UIColor(named: "availableColor")!
    static let unAvailableColor = UIColor(named: "unAvailableColor")!
    static let buttonBackgroundColor = UIColor(red: 17.0/255.0, green: 148.0/255.0, blue: 243.0/255.0, alpha: 1.0)
    static let textColor = UIColor(named: "navBarColor")!
}


struct DateFormat{
    static let dateFormat = "MM/dd/YYYY"
}


struct InternetAlert {
    static let kInternetMessage = AppHelper.localizedtext(key: "kInternetMessage")
}

struct ValidationAlert {
    static let kBankAccountNumber =  AppHelper.localizedtext(key: "kBankAccountNumber")
    static let kRoutingNumber =  AppHelper.localizedtext(key: "kRoutingNumber")
    static let kRoutingValidation =  AppHelper.localizedtext(key: "kRoutingValidation")
    static let kBankAccountValidation =  AppHelper.localizedtext(key: "kBankAccountValidation")
    static let kEmptyFirstName =  AppHelper.localizedtext(key: "kEmptyFirstName")
    static let kEmptyLastName =  AppHelper.localizedtext(key: "kEmptyLastName")
    static let kEmptyAliasName =  AppHelper.localizedtext(key: "kEmptyAliasName")
    static let kEmptyGender =  AppHelper.localizedtext(key: "kEmptyGender")
    static let kEmptyProfession =  AppHelper.localizedtext(key: "kEmptyProfession")
    static let kEmptyBio =  AppHelper.localizedtext(key: "kEmptyBio")
    static let kVideoAlert =  AppHelper.localizedtext(key: "kVideoAlert")
    static let kGOVIDAlert =  AppHelper.localizedtext(key: "kGovIDAlert")
    static let kEmptyEmailMSG = AppHelper.localizedtext(key: "kEmpltyEmailValidation")
    static let kEmailInvalidMSG = AppHelper.localizedtext(key: "kEmailValidation")
    static let kEmptyPasswordMSG = AppHelper.localizedtext(key: "kEmpltyPasswordValidation")
    static let kEmptyConfirmPasswordMSG = AppHelper.localizedtext(key: "kEmpltyConfirmPasswordValidation")
    static let kPasswordMismatchMSG = AppHelper.localizedtext(key: "kPasswordMismatch")
    static let kUserTypeMSG = AppHelper.localizedtext(key: "kUserType")
}

struct ErrorMessage{
    static let kNoData = "No data found!!"
    static let kEmptyPwd = "Please enter password."
    static let kEmptyConfirmPwd = "Please enter confirm password."
    static let kMatchPwd = "Password doesn't match."
    static let kEmptyEmail = "Please enter email address."
    static let kValidEmail = "Please enter valid email address."
    
}




