//
//  CustomTextField.swift
//  BaseProject
//
//  Created by TpSingh on 11/04/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit

class BWTextField: UITextField {

  override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
      if action == #selector(UIResponderStandardEditActions.paste(_:)) ||  action == #selector(UIResponderStandardEditActions.cut(_:)){
        return false
      }
      return super.canPerformAction(action, withSender: sender)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.updateView()
        //custom logic goes here
    }
   
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var paddingRight: CGFloat = 0
    
    @IBInspectable var rightImage : UIImage?{
        didSet{
            self.rightView = UIImageView(image: rightImage)
            self.clipsToBounds = true
            if let size = rightImage?.size {
                self.rightView?.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 10.0, height: size.height)
            }
            self.rightView?.contentMode = .center
            // select mode -> .never .whileEditing .unlessEditing .always
            self.rightViewMode = .always
        }
    }
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
   
    
   
    
    func updateView() {
        var bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: self.frame.height - 1, width: self.frame.width, height: 1.0)
        bottomLine.backgroundColor = UIColor.lightGray.cgColor
        self.borderStyle = .none
        self.layer.addSublayer(bottomLine)
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: COLOR.textColor])
    }
    
    
    
}
