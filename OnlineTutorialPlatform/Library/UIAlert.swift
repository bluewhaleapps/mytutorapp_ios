//
//  UIAlert.swift
//  UNMASK
//
//  Created by Reactive Space on 2/27/18.
//  Copyright © 2018 Owais Akram. All rights reserved.
//

import Foundation
import UIKit
// for showing alert

extension UIViewController{
    
    func showAlert(message: String, title: String)
    {
        let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
        let OkAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showErrorMsg(message:String)
    {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
//        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//        loadingIndicator.hidesWhenStopped = true
//        loadingIndicator.style = .gray
//        loadingIndicator.startAnimating();
//
//        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        // set the timer
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.hideIndicator), userInfo: nil, repeats: false)
    }
    
    
    
    func showIndicator(message:String)
    {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
                let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                loadingIndicator.hidesWhenStopped = true
                loadingIndicator.style = .gray
                loadingIndicator.startAnimating();
        
               alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        // set the timer
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.hideIndicator), userInfo: nil, repeats: false)
    }
    
    @objc func hideIndicator()
    {
        dismiss(animated: false, completion: nil)
        
    }
}
