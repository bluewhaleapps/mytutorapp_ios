 //
//  WSHandler.swift
//
//
//  Created by Bluewhale Apps on 26/06/19.
//  Copyright © 2019 Bluewhale Apps. All rights reserved.


import Foundation
 extension Data {
    
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
 }
 
extension Dictionary {
    func merge(_ dict: Dictionary<Key,Value>) -> Dictionary<Key,Value> {
        var mutableCopy = self
        for (key, value) in dict {
            mutableCopy[key] = value
        }
        return mutableCopy
    }
}
//---------------------------------------------------------------------
// MARK: PSRequest
//---------------------------------------------------------------------
class BWRequest: NSObject {
    
    var reqUrlComponent:String?
    var reqParam:Any?
    var reqParamArr:Array<Dictionary<String, Any>> = []
    var headerParam:Dictionary<String,String> = [:]
    var data: Data?
    init(reqUrlComponent:String) {
        self.reqUrlComponent = reqUrlComponent
    }
}

class BWResponse: NSObject {
    var response:URLResponse?
    var resData:Data?
    var error:Error?
    var jsonType:Int?
}

//---------------------------------------------------------------------
// MARK: WebService Handler
//---------------------------------------------------------------------
class BWHandler: NSObject,URLSessionDelegate {
    fileprivate static var obj:BWHandler?
    let kBASEURL  = WebServiceUrl.kBASEURL
    let kTimeOutValue = 60
   
   let sessionToken:String? = nil
    var apiUrl:URL?
    
    static func create() -> BWHandler{
        if(obj == nil){
            obj = BWHandler()
        }
        return obj!
    }
    
    static func sharedInstance() -> BWHandler{
        if(obj == nil){
            return create()
        }
        else{
            return obj!
        }
    }
    
    //---------------------------------------------------------------------
    // MARK: appDefaultHedaer
    //---------------BWResponse----------------------------------------
    func appDefaultHedaer() -> Dictionary<String,String>{
        let dic:Dictionary<String,String> = ["Content-Type":"application/json"]
        return dic
    }
    
    //---------------------------------------------------------------------
    // MARK: POST Web Service methods
    //---------------------------------------------------------------------
    
    func post(_ bwRequest:BWRequest,completionHandler:@escaping (BWResponse?) -> Void) -> Void {
        post(bwRequest,true, completionHandler: completionHandler )
    }
    
    func post(_ bwRequest:BWRequest,_ isShowDialog:Bool,completionHandler:@escaping (BWResponse?) -> Void) -> Void {
        let urlComponent = bwRequest.reqUrlComponent
        
        if urlComponent == nil {return}
        
        var urlSTR = ""
        
       urlSTR = kBASEURL + urlComponent!
        urlSTR = urlSTR.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!
        self.apiUrl = URL(string: urlSTR)
        var request = URLRequest(url: self.apiUrl!)
        request.httpMethod = "POST"
        request.timeoutInterval =  TimeInterval(kTimeOutValue);
        request.allHTTPHeaderFields = bwRequest.headerParam

        // Set request param
        let reqParam = bwRequest.reqParam
        if(reqParam != nil){
            do{
                let postData = try JSONSerialization.data(withJSONObject: reqParam, options:.prettyPrinted)
                let decoded = try JSONSerialization.jsonObject(with: postData, options: [])
                // here "decoded" is of type `Any`, decoded from JSON data
                let jsonString = String(data: postData, encoding: .utf8)
                print(jsonString!)
                // you can now cast it with the right type
                if decoded is [String:AnyObject] {
                    // use dictFromJSON
                }
                request.httpBody = postData
            }catch {
                let res = BWResponse()
                res.error = nil
                completionHandler(nil)
            }
        }
        
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)        
        let dataTask = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            
            if data != nil{
                let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("@API: RESPONCE:\(datastring!)")
                print("\n@API: -----------------------------------------------")
            }
            
            let res = BWResponse()
            res.response = response
            
            if let resError = error {
                res.error = resError
            }
            
            if let resData = data {
                res.resData = resData
            }
             completionHandler(res)
        })
        print("@API: -----------------------------------------------\n")
        print("@API: URL:\(urlSTR)\n")
        print("@API: PARAM:\(reqParam)\n")
        
        dataTask.resume()
    }
    
    
    //---------------------------------------------------------------------
    // MARK: GET Web Service methods
    //---------------------------------------------------------------------
    
    //==============================================================
    public func makeQueryString(values: Dictionary<String,Any>) -> String {
        var querySTR = ""
        if values.count > 0 {
            querySTR = "?"
            for item in values {
                let key = item.key
                let value = item.value as! String
                let keyValue = key + "=" + value + "&"
                querySTR = querySTR.appending(keyValue)
            }
            querySTR.removeLast()
        }
        return querySTR
    }
    
    func get(_ psRequest:BWRequest,_ isShowDialog:Bool,completionHandler:@escaping (BWResponse?) -> Void) -> Void {
        
        let urlComponent = psRequest.reqUrlComponent
        
        if urlComponent == nil {return}
        
        let dictData = psRequest.reqParam
        let param = dictData as! Dictionary<String,Any>
        let querySTR = makeQueryString(values: param)
    
        var urlSTR = kBASEURL + urlComponent! + querySTR
        
        urlSTR = urlSTR.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!
        let url = URL(string: urlSTR)
        var request = URLRequest(url: url!)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = psRequest.headerParam
        request.timeoutInterval =  180;

        
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
        let dataTask = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            
            if data != nil{
                let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("@API: RESPONCE:\(datastring!)")
                print("\n@API: -----------------------------------------------\n")
            }
            
            let res = BWResponse()
            res.response = response
            
            if let resError = error {
                res.error = resError
            }
            
            if let resData = data {
                res.resData = resData
            }
            
            //TODO: Send response back in BG thread and shift to main therad in controller only
            //DispatchQueue.main.async {
                completionHandler(res)
            //}
        })
        
        print("@API: -----------------------------------------------\n")
        print("@API: URL:\(urlSTR)\n")
        //print("@API: PARAM:\(params!)\n")
        dataTask.resume()
    }
    
    private func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
}
