//
//  UIView+Extension.swift
//  BaseProject
//
//  Created by Aj Mehra on 09/03/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import Foundation
import UIKit
let imageCache = NSCache<NSString, UIImage>()
extension UIImageView {
    func imageFromServerURL(_ URLString: String, placeHolder: UIImage?,size:CGSize?) {
            self.image = placeHolder
            if let cachedImage = imageCache.object(forKey: NSString(string: URLString)) {
                if(size != nil){
                    self.image = cachedImage.resizeImage(image: cachedImage, targetSize: size!)
                }else{
                    self.image = cachedImage;
                }
                return
            }
            
            if let url = URL(string: URLString) {
                URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                    
                    //print("RESPONSE FROM API: \(response)")
                    if error != nil {
                        DispatchQueue.main.async {
                            if(size != nil){
                                self.image = placeHolder!.resizeImage(image: placeHolder!, targetSize: size!)
                            }else{
                                self.image = placeHolder;
                            }
                            
                        }
                        return
                    }
                    DispatchQueue.main.async {
                        if let data = data {
                            if let downloadedImage = UIImage(data: data) {
                                imageCache.setObject(downloadedImage, forKey: NSString(string: URLString))
                                if(size != nil){
                                    self.image = downloadedImage.resizeImage(image: downloadedImage, targetSize: size!)
                                }else{
                                    self.image = downloadedImage;
                                }
                            }
                        }
                    }
                }).resume()
            }
        }
}


extension UIImage{
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else { return image }
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
    func convertImageToBase64(_ imageQuality: JPEGQuality) -> String {
        var imageData = self.jpegData(compressionQuality: imageQuality.rawValue)
        
            let strBase64:String = imageData?.base64EncodedString() ?? ""
            return strBase64
    }
}


extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            let color = UIColor.init(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = shadowRadius
        }
    }
    
    func glowRedText(radius:CGFloat) {
        let glowColor = UIColor.red
        self.layer.shadowColor = glowColor.cgColor
        self.layer.shadowRadius = radius;
        self.layer.shadowOpacity = 0.8;
        self.layer.shadowOffset = CGSize.zero;
        self.layer.masksToBounds = false;
    }
    
    
    func glowWhiteText(radius:CGFloat) {
        let glowColor = UIColor.gray
        self.layer.shadowColor = glowColor.cgColor
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = 0.5;
        self.layer.shadowOffset = CGSize.zero;
        self.layer.masksToBounds = false;
    }

}


